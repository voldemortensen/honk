import AppKit
import AVFoundation

final public class Honk: NSResponder, NSApplicationDelegate {
    var player: AVAudioPlayer!

    @objc public func applicationDidFinishLaunching(_ notification: Notification) {
        let opts = NSDictionary(object: true, forKey: kAXTrustedCheckOptionPrompt.takeUnretainedValue() as NSString) as CFDictionary
        guard AXIsProcessTrustedWithOptions(opts) == true else { return }
        NSEvent.addGlobalMonitorForEvents(matching: .keyDown, handler: self.handler)
        NSApplication.shared.activate(ignoringOtherApps: true)
    }

    public func handler(event: NSEvent) {
        if (event.isARepeat) {
            return
        }
        if (event.keyCode == 49) {
            do {
                let url = URL(fileURLWithPath: "./goose.mp3")
                player = try AVAudioPlayer(contentsOf: url)
                player.play()
            } catch {
            }
        }
    }

    @objc public func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
        return true
    }

    @objc func quit(_: Any) {
        NSApplication.shared.terminate(self)
    }
}

let mainApp = NSApplication.shared
let appController = Honk()
mainApp.delegate = appController
mainApp.run()
